package lux.pe.na;

import lombok.AllArgsConstructor;
import lux.pe.na.business.ConfigurationRedisService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@AllArgsConstructor
public class WorkSaveRedisApplication implements CommandLineRunner {

  private final ConfigurationRedisService configurationRedisService;

  public static void main(String[] args) {
    SpringApplication.run(WorkSaveRedisApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    configurationRedisService.saveConfigurationRedis();
  }
}
