package lux.pe.na.business;

import lux.pe.na.expose.response.ConfigurationResponse;
import reactor.core.publisher.Mono;

public interface ConfigurationRedisService {

  Mono<ConfigurationResponse> saveConfigurationRedis();
}
