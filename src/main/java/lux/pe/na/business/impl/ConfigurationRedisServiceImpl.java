package lux.pe.na.business.impl;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lux.pe.na.business.ConfigurationRedisService;
import lux.pe.na.expose.response.ConfigurationResponse;
import lux.pe.na.model.*;
import lux.pe.na.model.dto.*;
import lux.pe.na.redisclient.ConfigurationRedisClient;
import lux.pe.na.repository.*;
import lux.pe.na.webclient.ConfigurationWebClient;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class ConfigurationRedisServiceImpl implements ConfigurationRedisService {

  private final ConfigurationRedisClient configurationRedisClient;
  private final ConfigurationRepository configurationRepository;
  private final ConfigurationWebClient configurationWebClient;

  @Override
  public Mono<ConfigurationResponse> saveConfigurationRedis() {
    int isOk = 0;

    isOk = bankAsk(isOk);
    isOk = parameter(isOk);
    isOk = medicalCenter(isOk);
    isOk = ubigeo(isOk);
    isOk = masterTable(isOk);
    isOk = entity(isOk);
    isOk = entityAccountType(isOk);
    isOk = validationEntityAccount(isOk);
    isOk = productCoverage(isOk);
    isOk = investmentFund(isOk);
    isOk = sumInsured(isOk);
    isOk = productVersion(isOk);
    isOk = productReturn(isOk);
    isOk = packages(isOk);

    return save(isOk);
  }

  private int bankAsk(int isOk) {

    List<BancoPreguntaDto> bankAskDtoList = configurationRepository.bankAskDtoList();

    log.info("Se registró la lista BANCO_PREGUNTA con {} registros.",
        bankAskDtoList.size());

    if (configurationRedisClient.saveBankAsk(bankAskDtoList)) {
      log.info("Se registró la lista BANCO_PREGUNTA en  AZURE REDIS CACHE.");
    } else {
      log.info("No se registró la lista de BANCO_PREGUNTA en  AZURE REDIS CACHE.");
      isOk = -1;
    }

    return isOk;
  }

  private int parameter(int isOk) {

    List<ParametroDto> parameterDtoList = configurationRepository.parameterDtoList();

    log.info("Se registró la lista PARAMETRO con {} registros.",
        parameterDtoList.size());

    if (configurationRedisClient.saveParameter(parameterDtoList)) {
      log.info("Se registró la lista de PARAMETRO en  AZURE REDIS CACHE.");
    } else {
      log.info("No se registró la lista de PARAMETRO en  AZURE REDIS CACHE.");
      isOk = -1;
    }

    return isOk;
  }

  private int medicalCenter(int isOk) {
    List<CentroMedico> medicalCenterList = configurationRepository.findAll();

    log.info("Se registró la lista CENTRO_MEDICO con {} registros.",
        medicalCenterList.size());

    if (configurationRedisClient.saveMedicalCenter(medicalCenterList)) {
      log.info("Se registró la lista de CENTRO_MEDICO en  AZURE REDIS CACHE.");
    } else {
      log.info("No se registró la lista de CENTRO_MEDICO en  AZURE REDIS CACHE.");
      isOk = -1;
    }

    return isOk;
  }

  private int ubigeo(int isOk) {
    List<UbigeoDto> ubigeoDtoList = configurationRepository.ubigeoDtoList();

    log.info("Se registró la lista UBIGEO con {} registros.",
        ubigeoDtoList.size());

    if (configurationRedisClient.saveUbigeo(ubigeoDtoList)) {
      log.info("Se registró la lista de UBIGEO en  AZURE REDIS CACHE.");
    } else {
      log.info("No se registró la lista de UBIGEO en  AZURE REDIS CACHE.");
      isOk = -1;
    }

    return isOk;
  }

  private int masterTable(int isOk) {
    List<TablaMaestraDto> masterTableDtoList = configurationRepository.masterTableDtoList();

    log.info("Se registró la lista TABLA_MAESTRA con {} registros.",
        masterTableDtoList.size());

    if (configurationRedisClient.saveMasterTable(masterTableDtoList)) {
      log.info("Se registró la lista de TABLA_MAESTRA en  AZURE REDIS CACHE.");
    } else {
      log.info("No se registró la lista de TABLA_MAESTRA en  AZURE REDIS CACHE.");
      isOk = -1;
    }

    return isOk;
  }

  private int entity(int isOk) {
    List<EntidadDto> entityDtoList = configurationRepository.entityDtoList();

    log.info("Se registró la lista ENTIDAD con {} registros.",
        entityDtoList.size());

    if (configurationRedisClient.saveEntity(entityDtoList)) {
      log.info("Se registró la lista de ENTIDAD en  AZURE REDIS CACHE.");
    } else {
      log.info("No se registró la lista de ENTIDAD en  AZURE REDIS CACHE.");
      isOk = -1;
    }

    return isOk;
  }

  private int entityAccountType(int isOk) {
    List<TipoCuentaEntidadDto> entityAccountTypeDtoList = configurationRepository.entityAccountTypeDtoList();

    log.info("Se registró la lista TIPO_CUENTA_ENTIDAD con {} registros.",
        entityAccountTypeDtoList.size());

    if (configurationRedisClient.saveEntityAccountType(entityAccountTypeDtoList)) {
      log.info("Se registró la lista de TIPO_CUENTA_ENTIDAD en  AZURE REDIS CACHE.");
    } else {
      log.info("No se registró la lista de TIPO_CUENTA_ENTIDAD en  AZURE REDIS CACHE.");
      isOk = -1;
    }

    return isOk;
  }

  private int validationEntityAccount(int isOk) {
    List<CuentaEntidadValidacionDto> validationEntityAccountDtoList = configurationRepository.validationEntityAccountDtoList();

    log.info("Se registró la lista CUENTA_ENTIDAD_VALIDACION con {} registros.",
        validationEntityAccountDtoList.size());

    if (configurationRedisClient.saveValidationEntityAccount(validationEntityAccountDtoList)) {
      log.info("Se registró la lista de CUENTA_ENTIDAD_VALIDACION en  AZURE REDIS CACHE.");
    } else {
      log.info("No se registró la lista de CUENTA_ENTIDAD_VALIDACION en  AZURE REDIS CACHE.");
      isOk = -1;
    }

    return isOk;
  }

  private int productCoverage(int isOk) {
    ArrayList<CoberturaProductoDto> productsCoverage = new ArrayList<>(Objects.requireNonNull(configurationWebClient
            .getProductsCoverage()
            .block())
        .getCoberturaProductoDtoList());

    log.info("Registrando la lista COBERTURA_PRODUCTO con {} registros",
        (productsCoverage.size() > 0));

    if (configurationRedisClient.saveProductCoverage(productsCoverage)) {
      log.info("Se registró la lista COBERTURA_PRODUCTO en AZURE REDIS CACHE.");
    } else {
      log.info("No se registró la lista COBERTURA_PRODUCTO en AZURE REDIS CACHE.");
      isOk = -1;
    }
    return isOk;
  }

  private int investmentFund(int isOk) {
    ArrayList<FondoInversionDto> investmentFunds = new ArrayList<>(Objects.requireNonNull(configurationWebClient
            .getInvestmentFunds()
            .block())
        .getFondoInversionDtoList());

    log.info("Registrando la lista FONDOS_INVERSION con {} registros",
        (investmentFunds.size() > 0));

    if (configurationRedisClient.saveInvestmentFund(investmentFunds)) {
      log.info("Se registró la lista FONDOS_INVERSION en AZURE REDIS CACHE.");
    } else {
      log.info("No se registró la lista FONDOS_INVERSION en AZURE REDIS CACHE.");
      isOk = -1;
    }
    return isOk;
  }

  private int sumInsured(int isOk) {
    ArrayList<SumaAseguradaDto> sumsInsured = new ArrayList<>(Objects.requireNonNull(configurationWebClient
            .getSumsInsured()
            .block())
        .getSumaAseguradaDtoList());

    log.info("Registrando la lista SUMAS_ASEGURADAS con {} registros",
        (sumsInsured.size() > 0));

    if (configurationRedisClient.saveSumInsured(sumsInsured)) {
      log.info("Se registró la lista SUMAS_ASEGURADAS en AZURE REDIS CACHE.");
    } else {
      log.info("No se registró la lista SUMAS_ASEGURADAS en AZURE REDIS CACHE.");
      isOk = -1;
    }
    return isOk;
  }

  private int productVersion(int isOk) {
    ArrayList<ProductoVersionDto> productsVersion = new ArrayList<>(Objects.requireNonNull(configurationWebClient
            .getProductsVersion()
            .block())
        .getProductoVersionDtoList());

    log.info("Registrando la lista PRODUCTOS_VERSIONES con {} registros",
        (productsVersion.size() > 0));

    if (configurationRedisClient.saveProductVersion(productsVersion)) {
      log.info("Se registró la lista PRODUCTOS_VERSIONES en AZURE REDIS CACHE.");
    } else {
      log.info("No se registró la lista PRODUCTOS_VERSIONES en AZURE REDIS CACHE.");
      isOk = -1;
    }
    return isOk;
  }

  private int productReturn(int isOk) {
    ArrayList<RetornoProductoDto> productsReturn = new ArrayList<>(Objects.requireNonNull(configurationWebClient
            .getProductReturn()
            .block())
        .getRetornoProductoDtoList());

    log.info("Registrando la lista RETORNO_PRODUCTOS con {} registros",
        (productsReturn.size() > 0));

    if (configurationRedisClient.saveProductReturn(productsReturn)) {
      log.info("Se registró la lista RETORNO_PRODUCTOS en AZURE REDIS CACHE.");
    } else {
      log.info("No se registró la lista RETORNO_PRODUCTOS en AZURE REDIS CACHE.");
      isOk = -1;
    }
    return isOk;
  }

  private int packages(int isOk) {
    ArrayList<PaqueteDto> packagesDto = new ArrayList<>(Objects.requireNonNull(configurationWebClient
            .getPackages()
            .block())
        .getPaqueteDtoList());

    log.info("Registrando la lista PAQUETES con {} registros",
        (packagesDto.size() > 0));

    if (configurationRedisClient.savePackage(packagesDto)) {
      log.info("Se registró la lista PAQUETES en AZURE REDIS CACHE.");
    } else {
      log.info("No se registró la lista PAQUETES en AZURE REDIS CACHE.");
      isOk = -1;
    }
    return isOk;
  }

  private Mono<ConfigurationResponse> save(int isOK) {

    long seconds = System.currentTimeMillis();

    if (isOK == 0) {
      log.info("Se registraron todas las listas, ms: {}", (System.currentTimeMillis() - seconds));
      return Mono.just(ConfigurationResponse.builder()
          .responseCode(isOK)
          .responseMessage("Success")
          .build());
    } else {
      log.info("No se registraron las listas, ms: {}", (System.currentTimeMillis() - seconds));
      return Mono.just(ConfigurationResponse.builder()
          .responseCode(isOK)
          .responseMessage("Error")
          .build());
    }
  }
}
