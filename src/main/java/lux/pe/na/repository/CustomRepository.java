package lux.pe.na.repository;

import lux.pe.na.model.dto.*;

import java.util.List;

public interface CustomRepository {

  List<ParametroDto> parameterDtoList();

  List<TablaMaestraDto> masterTableDtoList();

  List<EntidadDto> entityDtoList();

  List<UbigeoDto> ubigeoDtoList();

  List<TipoCuentaEntidadDto> entityAccountTypeDtoList();

  List<CuentaEntidadValidacionDto> validationEntityAccountDtoList();

  List<BancoPreguntaDto> bankAskDtoList();
}
