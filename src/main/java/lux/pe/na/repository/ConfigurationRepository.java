package lux.pe.na.repository;

import lux.pe.na.model.CentroMedico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigurationRepository extends JpaRepository<CentroMedico, Integer>, CustomRepository {
}
