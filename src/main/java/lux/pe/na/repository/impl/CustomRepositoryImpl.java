package lux.pe.na.repository.impl;

import lux.pe.na.model.dto.*;
import lux.pe.na.repository.CustomRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

public class CustomRepositoryImpl implements CustomRepository {

  @PersistenceContext
  private EntityManager entityManager;


  @Override
  public List<ParametroDto> parameterDtoList() {

    String sql = "SELECT PR.IdParametro, PR.Descripcion,\n" +
        "PR.ValorCadena, PR.ValorNumerico\n" +
        "FROM Generales.PARAMETRO PR (NOLOCK)\n" +
        "WHERE (FechaCreacion > '19001201' OR\n" +
        "FechaModificacion > '190012101')";

    Query query = entityManager.createNativeQuery(sql, Tuple.class);
    List<Tuple> parameterList = query.getResultList();
    return parameterList.stream()
        .map(tuple -> ParametroDto.builder()
            .idParametro(Integer.parseInt(tuple.get("IdParametro").toString()))
            .descripcion(tuple.get("Descripcion").toString())
            .valorCadena(tuple.get("ValorCadena").toString())
            .valorNumerico(Double.parseDouble(tuple.get("ValorNumerico").toString()))
            .build())
        .collect(Collectors.toList());
  }

  @Override
  public List<TablaMaestraDto> masterTableDtoList() {
    String sql = "SELECT  T.IdTabla AS Id, I.NombreTabla AS Tabla, CodigoCampo,\n" +
        "ValorCadena AS Valor, ValorNumerico, ValorAuxiliarCadena AS ValorAuxiliar,\n" +
        "RTRIM(EquivalenciaVIAP) AS EquivalenciaVIAP\n" +
        "FROM Generales.TABLA_TABLAS (NOLOCK) T\n" +
        "INNER JOIN Generales.TABLA_INDICE I ON T.IdTabla = I.IdTabla\n" +
        "WHERE FlagActivo = 1\n" +
        "\n" +
        "UNION ALL \n" +
        "\n" +
        "SELECT 36 AS Id, 'Parentesco' AS Tabla, P.CodigoParentesco AS CodigoCampo,\n" +
        "P.Descripcion AS Valor, P.CodigoParentesco AS ValorNumerico,\n" +
        "CAST(ISNULL(P.CodigoSexo,2) AS VARCHAR) AS ValorAuxiliar,\n" +
        "CAST(AplicaFondoUniversitario AS VARCHAR) AS EquivalenciaVIAP\n" +
        "FROM Configuracion.PARENTESCO P (NOLOCK)\n" +
        "WHERE P.FlagActivo = 1\n" +
        "\n" +
        "UNION ALL\n" +
        "\n" +
        "SELECT 37 AS Id, 'TipoCobranzaRecurrente' AS Tabla,\n" +
        "CodigoTipoCobranza AS CodigoCampo, Descripcion AS Valor,\n" +
        "CodigoTipoCobranza AS ValorNumerico, '' AS ValorAuxiliar,\n" +
        "'' AS EquivalenciaVIAP\n" +
        "FROM Configuracion.TIPO_COBRANZA (NOLOCK)\n" +
        "WHERE AplicaPagoRecurrente = 1\n" +
        "\n" +
        "UNION ALL\n" +
        "\n" +
        "SELECT 38 AS Id, 'TipoCobranzaPrimerPago' AS Tabla,\n" +
        "CodigoTipoCobranza AS CodigoCampo, Descripcion AS Valor,\n" +
        "CodigoTipoCobranza AS ValorNumerico, '' AS ValorAuxiliar,\n" +
        "'' AS EquivalenciaVIAP\n" +
        "FROM Configuracion.TIPO_COBRANZA (NOLOCK)\n" +
        "WHERE AplicaPrimerPago = 1\n" +
        "\n" +
        "UNION ALL\n" +
        "\n" +
        "SELECT 39 AS Id, 'Pais' AS Tabla, CodigoPais AS CodigoCampo,\n" +
        "Descripcion AS Valor, CodigoPais AS ValorNumerico,\n" +
        "'' AS ValorAuxiliar, '' AS EquivalenciaVIAP\n" +
        "FROM Configuracion.PAIS\n" +
        "\n" +
        "UNION ALL\n" +
        "\n" +
        "SELECT 40 AS Id, 'Nacionalidad' AS Tabla, CodigoPais AS CodigoCampo,\n" +
        "Nacionalidad AS Valor, CodigoPais AS ValorNumerico,\n" +
        "'' AS ValorAuxiliar, '' AS EquivalenciaVIAP\n" +
        "FROM Configuracion.PAIS";

    Query query = entityManager.createNativeQuery(sql, Tuple.class);
    List<Tuple> masterTableList = query.getResultList();
    return masterTableList.stream()
        .map(tuple -> TablaMaestraDto.builder()
            .id(Integer.parseInt(tuple.get("Id").toString()))
            .tabla(tuple.get("Tabla").toString())
            .codigoCampo(Integer.parseInt(tuple.get("CodigoCampo").toString()))
            .valor(tuple.get("Valor").toString())
            .valorNumerico(Double.parseDouble(tuple.get("ValorNumerico").toString()))
            .valorAuxiliar(tuple.get("ValorAuxiliar").toString())
            .equivalenciaViap(tuple.get("EquivalenciaVIAP").toString())
            .build())
        .collect(Collectors.toList());
  }

  @Override
  public List<EntidadDto> entityDtoList() {
    String sql = "SELECT EN.CodigoEntidad, EN.Descripcion, EN.CodigoTipoCobranza,\n" +
        "EN.FechaCreacion, EN.FechaModificacion, EN.CodigoEntidadPPS,\n" +
        "EN.CodigoTajetaPPS, EN.FlagSincronizableSalud, EN.AplicaEPS,\n" +
        "EN.FlagAplicaEPS, EN.FlagActivo, EN.NumIdRelacionIngresoPPS,\n" +
        "EN.CodigoEntidadVisa\n" +
        "FROM Configuracion.ENTIDAD EN";

    Query query = entityManager.createNativeQuery(sql, Tuple.class);
    List<Tuple> entityList = query.getResultList();
    return entityList.stream()
        .map(tuple -> EntidadDto.builder()
            .codigoEntidad(Integer.parseInt(tuple.get("CodigoEntidad").toString()))
            .descripcion(tuple.get("Descripcion").toString())
            .codigoTipoCobranza(Integer.parseInt(tuple.get("CodigoTipoCobranza").toString()))
            .fechaCreacion(Timestamp.valueOf(tuple.get("FechaCreacion").toString()))
            .fechaModificacion(Timestamp.valueOf(tuple.get("FechaModificacion").toString()))
            .codigoEntidadPps(tuple.get("CodigoEntidadPPS").toString())
            .codigoTarjetaPps(tuple.get("CodigoTarjetaPPS").toString())
            .flagSincronizableSalud(Boolean.parseBoolean(tuple.get("FlagSincronizableSalud").toString()))
            .aplicaEps(Boolean.parseBoolean(tuple.get("AplicaEPS").toString()))
            .flagAplicaEps(Boolean.parseBoolean(tuple.get("FlagAplicaEPS").toString()))
            .flagActivo(Boolean.parseBoolean(tuple.get("FlagActivo").toString()))
            .numIdRelacionIngresoPps(tuple.get("NumIdRelacionIngresoPPS").toString())
            .codigoEntidadVisaClickApp(tuple.get("CodigoEntidadVisaClickApp").toString())
            .build())
        .collect(Collectors.toList());
  }

  @Override
  public List<UbigeoDto> ubigeoDtoList() {
    String sql = "SELECT DS.CodigoDistrito, DS.CodigoProvincia, DS.CodigoDepartamento,\n" +
        "DS.Descripcion AS Distrito, PV.Descripcion AS Provincia,\n" +
        "DP.Descripcion AS Departamento\n" +
        "FROM Configuracion.DISTRITO DS\n" +
        "INNER JOIN Configuracion.PROVINCIA PV \n" +
        "ON DS.CodigoProvincia = PV.CodigoProvincia\n" +
        "INNER JOIN Configuracion.DEPARTAMENTO DP\n" +
        "ON DP.CodigoDepartamento = DS.CodigoDepartamento\n" +
        "AND DP.CodigoDepartamento = PV.CodigoDepartamento";

    Query query = entityManager.createNativeQuery(sql, Tuple.class);
    List<Tuple> ubigeoList = query.getResultList();
    return ubigeoList.stream()
        .map(tuple -> UbigeoDto.builder()
            .codigoDistrito(Integer.parseInt(tuple.get("CodigoDistrito").toString()))
            .codigoProvincia(Integer.parseInt(tuple.get("CodigoProvincia").toString()))
            .codigoDepartamento(Integer.parseInt(tuple.get("CodigoDepartamento").toString()))
            .distrito(tuple.get("Distrito").toString())
            .provincia(tuple.get("Provincia").toString())
            .departamento(tuple.get("Departamento").toString())
            .build())
        .collect(Collectors.toList());
  }

  @Override
  public List<TipoCuentaEntidadDto> entityAccountTypeDtoList() {
    String sql = "SELECT TC.IdTipoCuentaEntidad, TC.CodigoEntidad, TC.CodigoTipoCuenta,\n" +
        "TC.Descripcion, TC.PlantillaNumeroCuenta, TC.CodigoMoneda, TC.FechaCreacion,\n" +
        "TC.FechaModificacion, TC.CodigoTipoCuentaPPS, TC.FlagSincronizableSalud,\n" +
        "TC.FlagActivoSalud\n" +
        "FROM Configuracion.TIPO_CUENTA_ENTIDAD TC";

    Query query = entityManager.createNativeQuery(sql, Tuple.class);
    List<Tuple> entityAccountTypeList = query.getResultList();
    return entityAccountTypeList.stream()
        .map(tuple -> TipoCuentaEntidadDto.builder()
            .idTipoCuentaEntidad(Integer.parseInt(tuple.get("IdTipoCuentaEntidad").toString()))
            .codigoEntidad(Integer.parseInt(tuple.get("CodigoEntidad").toString()))
            .codigoTipoCuenta(Integer.parseInt(tuple.get("CodigoTipoCuenta").toString()))
            .descripcion(tuple.get("Descripcion").toString())
            .plantillaNumeroCuenta(tuple.get("PlantillaNumeroCuenta").toString())
            .codigoMoneda(Integer.parseInt(tuple.get("CodigoMoneda").toString()))
            .fechaCreacion(Timestamp.valueOf(tuple.get("FechaCreacion").toString()))
            .fechaModificacion(Timestamp.valueOf(tuple.get("FechaModificacion").toString()))
            .codigoTipoCuentaPps(tuple.get("CodigoTipoCuentaPPS").toString())
            .flagSincronizableSalud(Boolean.parseBoolean(tuple.get("FlagSincronizableSalud").toString()))
            .flagActivoSalud(Boolean.parseBoolean(tuple.get("FlagActivoSalud").toString()))
            .build())
        .collect(Collectors.toList());
  }

  @Override
  public List<CuentaEntidadValidacionDto> validationEntityAccountDtoList() {
    String sql = "SELECT CE.IdCuentaEntidadValidacion, CE.CodigoValidacion, CE.CodigoEntidad,\n" +
        "CE.CodigoTipoCuenta, CE.PosicionInicial, CE.PosicionFinal, CE.CaracterValidacion,\n" +
        "CE.Descripcion, CE.FlagActivo, CE.FechaCreacion, CE.FechaModificacion\n" +
        "FROM Configuracion.CUENTA_ENTIDAD_VALIDACION CE";

    Query query = entityManager.createNativeQuery(sql, Tuple.class);
    List<Tuple> validationEntityAccountList = query.getResultList();
    return validationEntityAccountList.stream()
        .map(tuple -> CuentaEntidadValidacionDto.builder()
            .idCuentaEntidadValidacion(Integer.parseInt(tuple.get("IdCuentaEntidadValidacion").toString()))
            .codigoValidacion(Integer.parseInt(tuple.get("CodigoValidacion").toString()))
            .codigoEntidad(Integer.parseInt(tuple.get("CodigoEntidad").toString()))
            .codigoTipoCuenta(Integer.parseInt(tuple.get("CodigoTipoCuenta").toString()))
            .posicionInicial(Integer.parseInt(tuple.get("PosicionInicial").toString()))
            .posicionFinal(Integer.parseInt(tuple.get("PosicionFinal").toString()))
            .caracterValidador(tuple.get("CaracterValidador").toString())
            .descripcion(tuple.get("Descripcion").toString())
            .flagActivo(Boolean.parseBoolean(tuple.get("FlagActivo").toString()))
            .fechaCreacion(Timestamp.valueOf(tuple.get("FechaCreacion").toString()))
            .fechaModificacion(Timestamp.valueOf(tuple.get("FechaModificacion").toString()))
            .build())
        .collect(Collectors.toList());
  }

  @Override
  public List<BancoPreguntaDto> bankAskDtoList() {
    String sql = "SELECT BP.IdBancoPregunta, BP.CodigoTipoDeclaracion, BP.DescripcionPregunta,\n" +
        "BP.CodigoTipoPregunta, BP.OrdenApp, BP.OrdenFisicoSolicitud,\n" +
        "BP.OrdenSubIndiceFisicoSolicitud, BP.AplicaPreguntaMujer, BP.FechaCreacion,\n" +
        "BP.FechaModificacion, BP.EstadoSuscripcionInteligente, BP.OrdenSuscripcionInteligente\n" +
        "FROM Configuracion.BANCO_PREGUNTA BP";

    Query query = entityManager.createNativeQuery(sql, Tuple.class);
    List<Tuple> bankAskList = query.getResultList();
    return bankAskList.stream()
        .map(tuple -> BancoPreguntaDto.builder()
            .idBancoPregunta(Integer.parseInt(tuple.get("IdBancoPregunta").toString()))
            .codigoTipoDeclaracion(Integer.parseInt(tuple.get("CodigoTipoDeclaracion").toString()))
            .descripcionPregunta(tuple.get("DescripcionPregunta").toString())
            .codigoTipoPregunta(Integer.parseInt(tuple.get("CodigoTipoPregunta").toString()))
            .ordenApp(Integer.parseInt(tuple.get("OrdenApp").toString()))
            .ordenFisicoSolicitud(Integer.parseInt(tuple.get("OrdenFisicoSolicitud").toString()))
            .ordenSubIndiceFisicoSolicitud(Integer.parseInt(tuple.get("OrdenSubIndiceFisicoSolicitud").toString()))
            .aplicaPreguntaMujer(Boolean.parseBoolean(tuple.get("aplicaPreguntaMujer").toString()))
            .fechaCreacion(Timestamp.valueOf(tuple.get("FechaCreacion").toString()))
            .fechaModificacion(Timestamp.valueOf(tuple.get("FechaModificacion").toString()))
            .estadoSuscripcionInteligente(Integer.parseInt(tuple.get("EstadoSuscripcionInteligente").toString()))
            .ordenSuscripcionInteligente(Integer.parseInt(tuple.get("OrdenSuscripcionInteligente").toString()))
            .build())
        .collect(Collectors.toList());
  }


}
