package lux.pe.na.webclient;

import lux.pe.na.expose.response.*;
import reactor.core.publisher.Mono;

public interface ConfigurationWebClient {

  Mono<FondoInversionResponse> getInvestmentFunds();

  Mono<SumaAseguradaResponse> getSumsInsured();

  Mono<ProductoVersionResponse> getProductsVersion();

  Mono<RetornoProductoResponse> getProductReturn();

  Mono<PaqueteResponse> getPackages();

  Mono<CoberturaProductoResponse> getProductsCoverage();
}
