package lux.pe.na.webclient.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lux.pe.na.expose.response.*;
import lux.pe.na.webclient.ConfigurationWebClient;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@Slf4j
@AllArgsConstructor
public class ConfigurationWebClientImpl implements ConfigurationWebClient {

  private final WebClient webClient;

  @Override
  public Mono<FondoInversionResponse> getInvestmentFunds() {
    return webClient
        .get()
        .uri(uriBuilder ->
            uriBuilder
                .path("/fondosinversion")
                .build())
        .retrieve()
        .bodyToMono(FondoInversionResponse.class)
        .log();
  }

  @Override
  public Mono<SumaAseguradaResponse> getSumsInsured() {
    return webClient
        .get()
        .uri(uriBuilder ->
            uriBuilder
                .path("/sumasaseguradas")
                .build())
        .retrieve()
        .bodyToMono(SumaAseguradaResponse.class)
        .log();
  }

  @Override
  public Mono<ProductoVersionResponse> getProductsVersion() {
    return webClient
        .get()
        .uri(uriBuilder ->
            uriBuilder
                .path("/productosversiones")
                .build())
        .retrieve()
        .bodyToMono(ProductoVersionResponse.class)
        .log();
  }

  @Override
  public Mono<RetornoProductoResponse> getProductReturn() {
    return webClient
        .get()
        .uri(uriBuilder ->
            uriBuilder
                .path("/retornoproductos")
                .build())
        .retrieve()
        .bodyToMono(RetornoProductoResponse.class)
        .log();
  }

  @Override
  public Mono<PaqueteResponse> getPackages() {
    return webClient
        .get()
        .uri(uriBuilder ->
            uriBuilder
                .path("/paquetes")
                .build())
        .retrieve()
        .bodyToMono(PaqueteResponse.class)
        .log();
  }

  @Override
  public Mono<CoberturaProductoResponse> getProductsCoverage() {
    return webClient
        .get()
        .uri(uriBuilder ->
            uriBuilder
                .path("/coberturasproductos")
                .build())
        .retrieve()
        .bodyToMono(CoberturaProductoResponse.class)
        .log();
  }
}
