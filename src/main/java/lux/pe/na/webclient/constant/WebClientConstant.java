package lux.pe.na.webclient.constant;

public class WebClientConstant {

  public static final String CONTENT_TYPE = "Content-Type";
  public static final String APPLICATION_JSON = "application/json";
  public static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";

}
