package lux.pe.na.webclient.config;

import lux.pe.na.webclient.constant.WebClientConstant;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

import static lux.pe.na.webclient.constant.WebClientConstant.*;

@Configuration
public class WebClientApiConfig {

  @Value("${url}")
  private String investmentFundsBaseUrl;

  @Value("${subscriptionKey}")
  private String investmentFundsSubscriptionKey;

  @Bean
  public WebClient quoteWebclient() {
    return WebClient.builder()
        .exchangeStrategies(ExchangeStrategies.builder()
            .codecs(clientCodecConfigurer -> {
              clientCodecConfigurer
                  .defaultCodecs()
                  .enableLoggingRequestDetails(true);
            })
            .build()
        ).defaultHeader(SUBSCRIPTION_KEY, investmentFundsBaseUrl)
        .defaultHeader(CONTENT_TYPE, APPLICATION_JSON)
        .baseUrl(investmentFundsSubscriptionKey)
        .build();
  }
}
