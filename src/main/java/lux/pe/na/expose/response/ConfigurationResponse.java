package lux.pe.na.expose.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ConfigurationResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("CodigoRespuesta")
    private Integer responseCode;

    @JsonProperty("MensajeResponse")
    private String responseMessage;
}
