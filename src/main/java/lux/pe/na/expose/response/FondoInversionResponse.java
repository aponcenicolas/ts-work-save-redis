package lux.pe.na.expose.response;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.*;
import lux.pe.na.model.dto.FondoInversionDto;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class FondoInversionResponse {

  @JsonAlias({"data"})
  private List<FondoInversionDto> fondoInversionDtoList;
}
