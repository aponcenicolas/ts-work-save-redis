package lux.pe.na.expose.response;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.*;
import lux.pe.na.model.dto.SumaAseguradaDto;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class SumaAseguradaResponse {

  @JsonAlias({"data"})
  private List<SumaAseguradaDto> sumaAseguradaDtoList;
}
