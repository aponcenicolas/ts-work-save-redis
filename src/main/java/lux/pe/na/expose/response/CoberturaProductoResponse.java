package lux.pe.na.expose.response;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.*;
import lux.pe.na.model.dto.CoberturaProductoDto;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class CoberturaProductoResponse {

  @JsonAlias({"data"})
  private List<CoberturaProductoDto> coberturaProductoDtoList;
}
