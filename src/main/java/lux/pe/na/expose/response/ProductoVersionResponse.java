package lux.pe.na.expose.response;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.*;
import lux.pe.na.model.dto.ProductoVersionDto;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ProductoVersionResponse {

  @JsonAlias({"data"})
  private List<ProductoVersionDto> productoVersionDtoList;
}
