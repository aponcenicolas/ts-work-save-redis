package lux.pe.na.expose.response;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.*;
import lux.pe.na.model.dto.RetornoProductoDto;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class RetornoProductoResponse {

  @JsonAlias({"data"})
  private List<RetornoProductoDto> retornoProductoDtoList;
}
