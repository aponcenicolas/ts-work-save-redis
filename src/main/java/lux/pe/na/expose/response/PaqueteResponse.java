package lux.pe.na.expose.response;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.*;
import lux.pe.na.model.dto.PaqueteDto;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class PaqueteResponse {

  @JsonAlias({"data"})
  private List<PaqueteDto> paqueteDtoList;
}
