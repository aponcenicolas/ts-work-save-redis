package lux.pe.na.model.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RetornoProductoDto implements Serializable {
  private static final long serialVersionUID = 8689541081724630408L;

  private String codigoProducto;
  private String codigoVersionProducto;
  private Integer mesInicioPoliza;
  private Integer mesFinPoliza;
  private Integer anioInicioPoliza;
  private Integer anioFinPoliza;
  private Double porcentajeRetorno;
}
