package lux.pe.na.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ParametroDto implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonProperty("IdParametro")
  private Integer idParametro;

  @JsonProperty("Descripcion")
  private String descripcion;

  @JsonProperty("ValorCadena")
  private String valorCadena;

  @JsonProperty("ValorNumerico")
  private Double valorNumerico;
}
