package lux.pe.na.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CuentaEntidadValidacionDto implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonProperty("IdCuentaEntidadValidacion")
  private Integer idCuentaEntidadValidacion;

  @JsonProperty("CodigoValidacion")
  private Integer codigoValidacion;

  @JsonProperty("CodigoEntidad")
  private Integer codigoEntidad;

  @JsonProperty("CodigoTipoCuenta")
  private Integer codigoTipoCuenta;

  @JsonProperty("PosicionInicial")
  private Integer posicionInicial;

  @JsonProperty("PosicionFinal")
  private Integer posicionFinal;

  @JsonProperty("CaracterValidador")
  private String caracterValidador;

  @JsonProperty("Descripcion")
  private String descripcion;

  @JsonProperty("FlagActivo")
  private Boolean flagActivo;

  @JsonProperty("FechaCreacion")
  private Timestamp fechaCreacion;

  @JsonProperty("FechaModificacion")
  private Timestamp fechaModificacion;
}
