package lux.pe.na.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EntidadDto implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonProperty("CodigoEntidad")
  private Integer codigoEntidad;

  @JsonProperty("Descripcion")
  private String descripcion;

  @JsonProperty("CodigoTipoCobranza")
  private Integer codigoTipoCobranza;

  @JsonProperty("FechaCreacion")
  private Timestamp fechaCreacion;

  @JsonProperty("FechaModificacion")
  private Timestamp fechaModificacion;

  @JsonProperty("CodigoEntidadPPS")
  private String codigoEntidadPps;

  @JsonProperty("CodigoTarjetaPPS")
  private String codigoTarjetaPps;

  @JsonProperty("FlagSincronizableSalud")
  private Boolean flagSincronizableSalud;

  @JsonProperty("AplicaEPS")
  private Boolean aplicaEps;

  @JsonProperty("FlagAplicaEPS")
  private Boolean flagAplicaEps;

  @JsonProperty("FlagActivo")
  private Boolean flagActivo;

  @JsonProperty("NumIdRelacionIngresoPPS")
  private String numIdRelacionIngresoPps;

  @JsonProperty("CodigoEntidadVisaClickApp")
  private String codigoEntidadVisaClickApp;
}
