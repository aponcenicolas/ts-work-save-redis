package lux.pe.na.model.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PaqueteDto implements Serializable {
  private static final long serialVersionUID = 4704763162211420230L;

  private Integer codigoPaquete;
  private Integer codigoProducto;
  private String codigoVersionProducto;
  private Integer codigoTipoPaquete;
  private String descripcionTipoPaquete;
  private String condicionAsistencia;
  private String coberturaGeografica;
}
