package lux.pe.na.model.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductoVersionDto implements Serializable {
  private static final long serialVersionUID = 8528760787287219955L;

  private String codigoProducto;
  private String codigoVersionProducto;
  private Integer codigoLineaNegocio;
  private Integer codigoTipoProducto;
  private Integer periodoCobertura;
  private Integer codigoMoneda;
  private String descripcionProducto;
  private String nombreProducto;
  private String descripcionCotizador;
  private String descripcionVersionProducto;
  private String nombreVersionProducto;
  private Integer porcentajeRetornoPrima;
  private Integer numeroCuotasPagoBeneficio;
  private Boolean condicionAseguradosAdicionales;
  private Boolean condicionDevolucionTotal;
  private Boolean condicionPremiumLife;
  private Boolean condicionUniversityLife;

}
