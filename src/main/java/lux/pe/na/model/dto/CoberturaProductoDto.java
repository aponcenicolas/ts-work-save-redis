package lux.pe.na.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CoberturaProductoDto implements Serializable {
  private static final long serialVersionUID = -6998804034291856142L;

  private Integer idCoberturaProducto;
  private String codigoProducto;
  private String codigoVersionProducto;
  private Integer codigoCobertura;
  private String condicionCoberturaPrincipal;
  private String codigoReflex;
  private String codigoLineaNegocio;
  private Integer edadMaximaPermanencia;
  private Integer edadMaximaRenovacion;
  private Double porcentajeMaxSobremortalidad;
  private Integer capitalMinimo;
  private Integer capitalMaximo;
  private Double porcentajeMaxCobPrincipal;
  private String coberturaGratuita;
  private String condicionCumuloEvaluacion;
  private Double porcentajeExactoCoberturaPrincipal;
  private Integer edadContratacionInicio;
  private Integer edadContratacionFin;
  private Integer baseAplicacion;
  private Integer periodoCarencia;
  private Integer periodoFranquicia;
  private String condicionActivo;
  private String descripcionComercialCobertura;
  private Integer ordenComercialCobertura;
  private Integer codigoCoberturaRelacionada;
  private Boolean conCoberturaPrincipal;
  private Integer montoMinimoConCoberturaPrincipal;
  private String descripcionSistemaCobertura;
  private Integer codigoTipoCobertura;
  private Integer codigoTipoCoberturaCompania;

}
