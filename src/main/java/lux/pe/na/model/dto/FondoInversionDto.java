package lux.pe.na.model.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FondoInversionDto implements Serializable {
  private static final long serialVersionUID = 5069007165252308541L;

  private Integer codigo;
  private String descripcion;
}
