package lux.pe.na.model.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SumaAseguradaDto implements Serializable {
  private static final long serialVersionUID = 555940936624430345L;

  private Integer codigo;
  private String descripcion;
}
