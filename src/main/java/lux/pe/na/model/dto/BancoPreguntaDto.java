package lux.pe.na.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BancoPreguntaDto implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonProperty("IdBancoPregunta")
  private Integer idBancoPregunta;

  @JsonProperty("CodigoTipoDeclaracion")
  private Integer codigoTipoDeclaracion;

  @JsonProperty("DescripcionPregunta")
  private String descripcionPregunta;

  @JsonProperty("CodigoTipoPregunta")
  private Integer codigoTipoPregunta;

  @JsonProperty("OrdenApp")
  private Integer ordenApp;

  @JsonProperty("OrdenFisicoSolicitud")
  private Integer ordenFisicoSolicitud;

  @JsonProperty("OrdenSubIndiceFisicoSolicitud")
  private Integer ordenSubIndiceFisicoSolicitud;

  @JsonProperty("AplicaPreguntaMujer")
  private Boolean aplicaPreguntaMujer;

  @JsonProperty("FechaCreacion")
  private Timestamp fechaCreacion;

  @JsonProperty("FechaModificacion")
  private Timestamp fechaModificacion;

  @JsonProperty("EstadoSuscripcionInteligente")
  private Integer estadoSuscripcionInteligente;

  @JsonProperty("OrdenSuscripcionInteligente")
  private Integer ordenSuscripcionInteligente;
}
