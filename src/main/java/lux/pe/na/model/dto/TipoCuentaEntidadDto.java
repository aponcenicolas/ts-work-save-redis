package lux.pe.na.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.Column;
import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TipoCuentaEntidadDto implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonProperty("IdTipoCuentaEntidad")
  private Integer idTipoCuentaEntidad;

  @JsonProperty("CodigoEntidad")
  private Integer codigoEntidad;

  @JsonProperty("CodigoTipoCuenta")
  private Integer codigoTipoCuenta;

  @JsonProperty("Descripcion")
  private String descripcion;

  @JsonProperty("PlantillaNumeroCuenta")
  private String plantillaNumeroCuenta;

  @JsonProperty("CodigoMoneda")
  private Integer codigoMoneda;

  @JsonProperty("FechaCreacion")
  private Timestamp fechaCreacion;

  @JsonProperty("FechaModificacion")
  private Timestamp fechaModificacion;

  @JsonProperty("CodigoTipoCuentaPPS")
  private String codigoTipoCuentaPps;

  @JsonProperty("FlagSincronizableSalud")
  private Boolean flagSincronizableSalud;

  @JsonProperty("FlagActivoSalud")
  private Boolean flagActivoSalud;
}
