package lux.pe.na.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UbigeoDto implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonProperty("CodigoDistrito")
  private Integer codigoDistrito;

  @JsonProperty("CodigoProvincia")
  private Integer codigoProvincia;

  @JsonProperty("CodigoDepartamento")
  private Integer codigoDepartamento;

  @JsonProperty("Distrito")
  private String distrito;

  @JsonProperty("Provincia")
  private String provincia;

  @JsonProperty("Departamento")
  private String departamento;
}
