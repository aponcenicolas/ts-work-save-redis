package lux.pe.na.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TablaMaestraDto implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonProperty("Id")
  private Integer id;

  @JsonProperty("Tabla")
  private String tabla;

  @JsonProperty("CodigoCampo")
  private Integer codigoCampo;

  @JsonProperty("Valor")
  private String valor;

  @JsonProperty("ValorNumerico")
  private Double valorNumerico;

  @JsonProperty("ValorAuxiliar")
  private String valorAuxiliar;

  @JsonProperty("EquivalenciaVIAP")
  private String equivalenciaViap;
}
