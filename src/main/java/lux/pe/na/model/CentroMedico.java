package lux.pe.na.model;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "Centro_Medico", schema = "Configuracion")
public class CentroMedico implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "CodigoCentroMedico")
  private Integer codigoCentroMedico;

  @Column(name = "Descripcion", length = 200)
  private String descripcion;

  @Column(name = "Direccion", length = 120)
  private String direccion;

  @Column(name = "CodigoDepartamento")
  private Integer codigoDepartamento;

  @Column(name = "CodigoProvincia")
  private Integer codigoProvincia;

  @Column(name = "CodigoDistrito")
  private Integer codigoDistrito;

  @Column(name = "FlagAtencionDomicilio")
  private Boolean flagAtencionDomicilio;

  @Column(name = "FlagActivo")
  private Boolean flagActivo;

  @Column(name = "FechaCreacion")
  private Timestamp fechaCreacion;

  @Column(name = "FechaModificacion")
  private Timestamp fechaModificacion;

}
