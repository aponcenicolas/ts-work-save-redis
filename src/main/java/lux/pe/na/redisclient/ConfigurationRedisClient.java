package lux.pe.na.redisclient;

import lux.pe.na.model.*;
import lux.pe.na.model.dto.*;

import java.util.List;

public interface ConfigurationRedisClient {

  boolean saveBankAsk(List<BancoPreguntaDto> bankAskDtoList);

  boolean saveParameter(List<ParametroDto> parameterDtoList);

  boolean saveMedicalCenter(List<CentroMedico> medicalCenterList);

  boolean saveUbigeo(List<UbigeoDto> ubigeoDtoList);

  boolean saveMasterTable(List<TablaMaestraDto> masterTableDtoList);

  boolean saveEntity(List<EntidadDto> entityDtoList);

  boolean saveEntityAccountType(List<TipoCuentaEntidadDto> entityAccountTypeDtoList);

  boolean saveValidationEntityAccount(List<CuentaEntidadValidacionDto> validationEntityAccountDtoList);

  boolean saveProductCoverage(List<CoberturaProductoDto> productCoverageDtoList);

  boolean saveInvestmentFund(List<FondoInversionDto> investmentFundsDtoList);

  boolean saveSumInsured(List<SumaAseguradaDto> sumInsuredDtoList);

  boolean saveProductVersion(List<ProductoVersionDto> productVersionDtoList);

  boolean saveProductReturn(List<RetornoProductoDto> ProductReturnDtoList);

  boolean savePackage(List<PaqueteDto> packageDtoList);
}
