package lux.pe.na.redisclient.impl;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lux.pe.na.model.*;
import lux.pe.na.model.dto.*;
import lux.pe.na.redisclient.ConfigurationRedisClient;
import lux.pe.na.redisclient.config.RedisKeyNames;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.List;

@Repository
@Slf4j
@RequiredArgsConstructor
public class ConfigurationRedisClientImpl implements ConfigurationRedisClient {

  private final StringRedisTemplate redisTemplate;
  private ValueOperations<String, String> valueOperations;

  @PostConstruct
  private void init() {
    valueOperations = redisTemplate.opsForValue();
  }

  @Override
  public boolean saveBankAsk(List<BancoPreguntaDto> bankAskDtoList) {
    return saveListOnRedis(RedisKeyNames.BANCO_PREGUNTA.getKey(), bankAskDtoList);
  }

  @Override
  public boolean saveParameter(List<ParametroDto> parameterDtoList) {
    return saveListOnRedis(RedisKeyNames.PARAMETRO.getKey(), parameterDtoList);
  }

  @Override
  public boolean saveMedicalCenter(List<CentroMedico> medicalCenterList) {
    return saveListOnRedis(RedisKeyNames.CENTRO_MEDICO.getKey(), medicalCenterList);
  }

  @Override
  public boolean saveUbigeo(List<UbigeoDto> ubigeoDtoList) {
    return saveListOnRedis(RedisKeyNames.UBIGEO.getKey(), ubigeoDtoList);
  }

  @Override
  public boolean saveMasterTable(List<TablaMaestraDto> masterTableDtoList) {
    return saveListOnRedis(RedisKeyNames.TABLA_MAESTRA.getKey(), masterTableDtoList);
  }

  @Override
  public boolean saveEntity(List<EntidadDto> entityDtoList) {
    return saveListOnRedis(RedisKeyNames.ENTIDAD.getKey(), entityDtoList);
  }

  @Override
  public boolean saveEntityAccountType(List<TipoCuentaEntidadDto> entityAccountTypeDtoList) {
    return saveListOnRedis(RedisKeyNames.TIPO_CUENTA_ENTIDAD.getKey(), entityAccountTypeDtoList);
  }

  @Override
  public boolean saveValidationEntityAccount(List<CuentaEntidadValidacionDto> validationEntityAccountDtoList) {
    return saveListOnRedis(RedisKeyNames.CUENTA_ENTIDAD_VALIDACION.getKey(), validationEntityAccountDtoList);
  }

  @Override
  public boolean saveProductCoverage(List<CoberturaProductoDto> productCoverageDtoList) {
    return saveListOnRedis(RedisKeyNames.COBERTURA_PRODUCTO.getKey(), productCoverageDtoList);
  }

  @Override
  public boolean saveInvestmentFund(List<FondoInversionDto> investmentFundsDtoList) {
    return saveListOnRedis(RedisKeyNames.FONDOS_INVERSION.getKey(), investmentFundsDtoList);
  }

  @Override
  public boolean saveSumInsured(List<SumaAseguradaDto> sumInsuredDtoList) {
    return saveListOnRedis(RedisKeyNames.SUMAS_ASEGURADAS.getKey(), sumInsuredDtoList);
  }

  @Override
  public boolean saveProductVersion(List<ProductoVersionDto> productVersionDtoList) {
    return saveListOnRedis(RedisKeyNames.PRODUCTOS_VERSIONES.getKey(), productVersionDtoList);
  }

  @Override
  public boolean saveProductReturn(List<RetornoProductoDto> ProductReturnDtoList) {
    return saveListOnRedis(RedisKeyNames.RETORNO_PRODUCTOS.getKey(), ProductReturnDtoList);
  }

  @Override
  public boolean savePackage(List<PaqueteDto> packageDtoList) {
    return saveListOnRedis(RedisKeyNames.PAQUETES.getKey(), packageDtoList);
  }

  private boolean saveListOnRedis(String keyName, List<?> list) {
    boolean value = true;
    try {
      ObjectMapper mapper = new ObjectMapper();
      String json = mapper.writeValueAsString(list);
      valueOperations.set(keyName, json);
    } catch (JsonProcessingException e) {
      log.error(e.getMessage(), e);
      value = false;
    }
    return value;
  }
}
